import React from "react";
import Link from "next/link";

const Nav = ({ categories }) => {
  return (
    <div className="header-block">
        <div className="leftTitle">
            <h1>{"Casa dei Fiori"}</h1>
        </div>
        <div className="rightTitle">
            <h3>Цветочный магазин: улица Лесников, 27, Красноярск</h3>
            <h3>Часы работы: Ежедневно c 10:00 по 22:00</h3>

            <h2>ОНЛАЙН ВИТРИНА</h2>
        </div>
    </div>
  );
};

export default Nav;