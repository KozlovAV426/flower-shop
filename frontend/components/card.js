import React from "react";
import Link from "next/link";
import Image from "./image";

const Card = ({ item }) => {
  return (
    // <Link href={`/article/${item.id}`}>
        <div className="uk-card uk-card-muted">
        <div className="uk-card-body">
            <p id="title" className="uk-text-uppercase">
              {item.attributes.name}
            </p>
            {/* <p id="description" className="uk-text-large">
              {item.attributes.description}
            </p> */}
          </div>
          <div className="uk-card-media-top">
            <Image image={item.attributes.photo} />
          </div>
        </div>
    // </Link>
  );
};

export default Card;