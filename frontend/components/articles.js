import React from "react";
import Card from "./card";

const Items = ({ items }) => {
  const leftItmesCount = Math.ceil(items.length / 2);
  const leftItems = items.slice(0, leftItmesCount);
  const rightItems = items.slice(leftItmesCount, items.length);

  return (
    <div>
      <div className="uk-child-width-1-2@s" data-uk-grid="true">
        <div>
          {leftItems.map((item, i) => {
            return (
              <Card
                item={item}
                key={`article__left__${item.id}`}
              />
            );
          })}
        </div>
        <div>
            {rightItems.map((item, i) => {
              return (
                <Card
                  item={item}
                  key={`article__right__${item.id}`}
                />
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default Items;