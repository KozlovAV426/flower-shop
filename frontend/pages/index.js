import React from "react";
import Items from "../components/articles";
import Layout from "../components/layout";
import { fetchAPI } from "../lib/api";
import Head from 'next/head'


const Home = ({ items, categories}) => {
  return (
    <Layout categories={categories}>
    <Head>
      <title>
        Цветы Красноярск
      </title>
    </Head>
      <div className="uk-section">
        <div className="uk-container uk-container-large">
          <Items items={items} />
        </div>
      </div>
    </Layout>
  );
};

export async function getStaticProps() {
  // Run API calls in parallel
  
  
  try {
    const [articlesRes] = await Promise.all([
    fetchAPI("/items", { populate: ["photo"] })]);
    return {
      props: {
        items: articlesRes.data,
        categories: ['flowers']
      },
      revalidate: 1,
    };
  } catch {
    return {
      props: {
        items: [],
        categories: ['flowers']
      },
      revalidate: 1,
    };
  }


}

export default Home;